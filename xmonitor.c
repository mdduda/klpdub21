/*
 * Copyright (c) 2006, Peek Traffic B.V.
 *
 * File: MONITOR.C
 *
 * Description:
 *
 *   Module for communication with central monitoring server.
 *
 * Decisions:
 *
 *  T:XMONITOR.INI          Initialisation function.
 *  V:XMONITOR.STS          Some debug counters.
 *
 * History:
 *
 *   --CREDAT--
 *   2006-09-25
 *	 2008-04-30				monitor_create_frame function does not work under WIN32 anymore
 *	 2011-07-12				use socket control functions instead of XSOCKC server
 *   --UPDATE--
 */

/******************************************************************************/
/* Include files                                                              */
/******************************************************************************/

#include "vor.h"

/******************************************************************************/
/* Macro constant definitions                                                 */
/******************************************************************************/

#define MONITOR_MSG_LEN           				77

#define MONITOR_STS_MSG_OUT       				0
#define MONITOR_ERR_OPEN_SOCKET    				1  
#define MONITOR_ERR_SEND_DATA    				2
#define MONITOR_SOCKET_ERR_NUMBER				3
#define MONITOR_STS_OPEN_ID		  				4
#define MONITOR_STS_OPEN_TIME	  				5
#define MONITOR_STS_OPEN_DATE	  				6
#define MONITOR_STS_OPEN_PLAN     				7
#define MONITOR_STS_OPEN_PLAN_NAME  			8
#define MONITOR_STS_OPEN_STATUS   				9
#define MONITOR_STS_OPEN_ALARMS   				10
#define MONITOR_XLIB_CLOCK						11
#define MONITOR_T_LAST_DATA_SENT				12
#define MONITOR_TIME_DIFF						13
#define MONITOR_NUM_STS           				14

#define MONITOR_CLIENT_INIT                     0
#define MONITOR_CLIENT_OPEN_SOCKET			    1
#define MONITOR_CLIENT_SEND_DATA			    2

#define XMONITOR_NAME_INI       			   "XMONITOR.INI"

/******************************************************************************/
/* Macro operation definitions                                                */
/******************************************************************************/

#define MONITOR_INCR_COUNTER(monitor, counter) \
{ if (++monitor.status[counter] >= (int32)1000) monitor.status[counter] = 0; }

/******************************************************************************/
/* Type definitions                                                           */
/******************************************************************************/

typedef struct {
  int         iState;					 	/* connection state */
  void       *iSocket;
  int		  remotePort;
  char        remoteIp[20];
  char        statusMsg[MONITOR_MSG_LEN];
  char		  statusMsgCpy[MONITOR_MSG_LEN];
  int32		  http_sec_tmr;		 			/* timer for setting frame sending frequency */
  int		  new_monitoring_data;
  int		  control_sum;

  void       *wdg_handle;
  
  int32       status[MONITOR_NUM_STS];    	/* status counters */
} MONITOR_SERVER_DATA;

/******************************************************************************/
/* Private operation declarations                                             */
/******************************************************************************/

static int    monitor_init_func(void *user_data, int pos, const char *data);
static int 	  monitor_socket_client(int iArg);
static void   monitor_1s_callback(void *user_data);
static int	  monitor_calculate_crc(void);

static int 	  monitor_socket_open(void);
static void	  monitor_socket_close(void);

static void   monitor_create_frame(void);

static void   monitor_get_id(int *controller_id); 
static void   monitor_get_time(int *controller_time);
static void   monitor_get_date(int *controller_date);
static void   monitor_get_plan(int *controller_plan);
static void   monitor_get_plan_name(int *controller_plan_name);
static void   monitor_get_alarms(int *controller_alarms);
static void   monitor_get_status(int *controller_status);

/******************************************************************************/
/* Data definitions                                                           */
/******************************************************************************/

/* { name, flags, uicmask, log, idx, nrel} , res, min, max, getf, putf, data */

static const PARV_CREATE_STRUC monitor_parv_def = {
  { "XMONITOR.STS", 0, UIC_M_RRR, FALSE, NULL, MONITOR_NUM_STS },
  1, 0, 999,
  PARV_get_f_int32, PARV_put_f_int32, NULL };


static   MONITOR_SERVER_DATA   monitor;
  
  
/******************************************************************************/
/* Global operation definitions                                               */
/******************************************************************************/

/*
 * Operation: XMONITOR_init
 *
 * Abstract: Initialisation of the MONITOR server.
 *
 * Decisions:    -
 *
 */

int
XMONITOR_init()
{
  return (USER_add_function(XMONITOR_NAME_INI, monitor_init_func, NULL, UIC_M_SU));
}

/******************************************************************************/
/* Private operation definitions                                              */
/******************************************************************************/

/*
 * Operation: monitor_init_func
 *
 * Abstract:
 *
 * Decisions:
 *
 *    - P=XCOMxxx/0: serial line via XCOM server.
 */

static int
monitor_init_func(void *user_data, int pos, const char *data)
{
  
  ARG_USED(user_data);
  ARG_USED(pos);
  ARG_USED(data);
  
  /* Clear settings */
  memset(&monitor, 0, sizeof(monitor));
  
  /* Get the IP address and port of the monitoring server */
  monitor.remotePort = (int)Xargtoi(data, "PORT", 1);
   
  if (!Xargtoa(data, "IP", NULL, monitor.remoteIp))
  {
	  return (FALSE);
  }
   
  /* Create the MONITOR status parameter in the database */
  (void)XLIB_parv_create_1(&monitor_parv_def, 1, NULL, 0, monitor.status);

  /* Create the client task */ 
  monitor.wdg_handle = XLIB_watchdog_open("MONITORING", 3000, XLIB_WDG_ACTION_SOFT_RESET);
  XLIB_task_create("MONITORING", XLIB_LOW3_PRIO, XLIB_STACK_MEDIUM, monitor_socket_client, 0);
  
  #if !(defined(TPS) || defined(WIN32))
  
  XLIB_tmr_event(10, monitor_1s_callback, NULL, 0);
  
  #endif
  
  return (TRUE);
}

static int
monitor_socket_client(int iArg)
{
	int i;
    ARG_USED(iArg);
    
	/* Wait for system to be fully initialized */
	XLIB_task_wait_init_completed();
	
	while (XLIB_TASKS_RUNNING)
	{
		XLIB_watchdog_trigger(monitor.wdg_handle);
		if (monitor.new_monitoring_data == TRUE)
		{
			XLIB_enter_critical_section();
			
			for (i=0;i<=sizeof(monitor.statusMsg);i++)
  			{
	  			monitor.statusMsgCpy[i] = monitor.statusMsg[i];
  			}
			
			XLIB_leave_critical_section();
			  
			switch (monitor.iState)
			{
				case MONITOR_CLIENT_INIT:
      			{
	      			monitor_socket_close();                     /* Make sure all sockets are closed */
      				monitor.iState = MONITOR_CLIENT_OPEN_SOCKET;
					break;
			    }  
				case MONITOR_CLIENT_OPEN_SOCKET:
				{
					if(monitor_socket_open())
					{
						monitor.status[MONITOR_ERR_OPEN_SOCKET] = 0;
						monitor.iState = MONITOR_CLIENT_SEND_DATA;
					}
					else
					{
						MONITOR_INCR_COUNTER(monitor, MONITOR_ERR_OPEN_SOCKET);
						monitor.status[MONITOR_SOCKET_ERR_NUMBER] = XLIB_sock_get_last_error(monitor.iSocket);
						monitor.iState = MONITOR_CLIENT_INIT;
					}
					break;
				}
			    case MONITOR_CLIENT_SEND_DATA:
	      		{
		      		if (XLIB_sock_send(monitor.iSocket, monitor.statusMsgCpy, strlen(monitor.statusMsgCpy)+1) < 0)
        			{
	        			monitor.status[MONITOR_STS_MSG_OUT] = 0;
	        			MONITOR_INCR_COUNTER(monitor, MONITOR_ERR_SEND_DATA);
	        			monitor.status[MONITOR_SOCKET_ERR_NUMBER] = XLIB_sock_get_last_error(monitor.iSocket);
          				monitor_socket_close();
          				monitor.iState = MONITOR_CLIENT_INIT;
        			}
        			else
        			{
	        			monitor.status[MONITOR_SOCKET_ERR_NUMBER] = 0;
	        			monitor.status[MONITOR_ERR_SEND_DATA] = 0;
	        			MONITOR_INCR_COUNTER(monitor, MONITOR_STS_MSG_OUT);
	        			monitor.status[MONITOR_T_LAST_DATA_SENT] = ((int)XLIB_clock()/10);
	        			monitor.new_monitoring_data = 0;
          				monitor_socket_close();       
          				monitor.iState = MONITOR_CLIENT_INIT;
      				}
      				break;
  				}
  			}

  			XLIB_task_delay(1000);       
		}
		else
		{
			XLIB_task_delay(1000);
		}
	}
return(FALSE);
}
			
/*
 * Operation : monitor_1s_callback
 *
 * Abstract  : Collects status data of the controller.
 *
 * Decisions : -
 *  
 *
 */

static void
monitor_1s_callback(void *user_data)
{
  static  PAR_INST  parm_pari;
  int http_sec;
  int calculated_crc;
 
  if (!XPARV_openi("HTTP_SEC", "PARM.R1", &parm_pari))
  {
	  http_sec = 20;
  }
  else
  {
	  http_sec = PARV_get(parm_pari.id, parm_pari.el, UIC_APPLICATION);
  }

  monitor_create_frame();
  calculated_crc = monitor_calculate_crc();
  
  if (monitor.control_sum != calculated_crc)
  {
	  monitor.new_monitoring_data = 1;
	  monitor.control_sum = calculated_crc;
  }
  
  if (((++monitor.http_sec_tmr) > http_sec) && (http_sec != 0))
  {
	  monitor.http_sec_tmr = 0;
	  monitor.new_monitoring_data = 1;
  }
  monitor.status[MONITOR_XLIB_CLOCK] = ((int)XLIB_clock()/10);        				
  monitor.status[MONITOR_TIME_DIFF] = ((int)XLIB_clock()/10) - monitor.status[MONITOR_T_LAST_DATA_SENT];    
}

/*
 * Operation: monitor_create_frame
 *
 * Abstract:  Creates a frame message.
 *
 * Decisions: 
 *
 */

static void
monitor_create_frame(void)
{
  int		controller_id[8], controller_time[6], controller_date[6], controller_plan[3],
  		    controller_plan_name[14], controller_alarms[32], controller_status[1];
  char		frame[77];
  int 		i;
   	 
  monitor_get_id(controller_id);
  monitor_get_time(controller_time);
  monitor_get_date(controller_date);
  monitor_get_plan(controller_plan);
  monitor_get_plan_name(controller_plan_name);
  monitor_get_alarms(controller_alarms);
  monitor_get_status(controller_status);
 
  for (i=0; i<8; i++)
  {
	  frame[i] = controller_id[i];
  }
  frame[8]  = '!';
  for (i=0; i<6; i++)
  {
	  frame[i+9] = controller_time[i];
  }
  frame[15] = '!';
  for (i=0; i<6; i++)
  {
	  frame[i+16] = controller_date[i];
  }
  frame[22] = '!';
  for (i=0; i<3; i++)
  {
	  frame[i+23] = controller_plan[i];
  }
  frame[26] = '!';
  for (i=0; i<14; i++)
  {
	  frame[i+27] = controller_plan_name[i];
  }
  frame[41] = '!';
  for (i=0; i<32; i++)
  {
	  frame[i+42] = controller_alarms[i];
  }
  frame[74] = '!';
  for (i=0; i<1; i++)
  {
	  frame[i+75] = controller_status[i];
  }
  frame[76] = '!';
  
  for (i=0;i<=sizeof(frame);i++)
  {
	  monitor.statusMsg[i] = frame[i];
  }
}

static void
monitor_get_id(int *controller_id)
{
void   *handle;
char   buf[8];
char   buf2[8];
int    cnt=0,i=0;

        handle = XPART_open("VEEM.SYS");
        if ( ! handle)
  		{
      		monitor.status[MONITOR_STS_OPEN_ID] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_ID] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_ID] = 0;
 			  } 
		}
		sprintf(buf2, "%s", PART_get(handle, cnt, buf, UIC_APPLICATION));

		  do
		  {
			  controller_id[i] = buf2[i];	  
			  i++;
		  }
		  while (buf2[i] != 0);
		 
		  for (; i<8; i++)
		  {
			  controller_id[i] = '#';	  
		  }
}


static void
monitor_get_time(int *controller_time)
{
void   *handle;
int    cnt=0,i=0;
char   buf[6];
	
		handle = XPARV_open("TIME.SYS");
        if ( ! handle)
  		{
      		monitor.status[MONITOR_STS_OPEN_TIME] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_TIME] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_TIME] = 0;
 			  } 
		}
		  sprintf(buf, "%06d", (int)PARV_get(handle, cnt, UIC_APPLICATION));
  		  
		  for (i=0; i<6; i++)
		  {
			  controller_time[i] = buf[i];	  
		  }
}


static void
monitor_get_date(int *controller_date)
{
void   *handle;
int    cnt=0,i=0;
char   buf[6];
	
		handle = XPARV_open("DATE.SYS");
        if ( ! handle)
                      
  		{
      		monitor.status[MONITOR_STS_OPEN_DATE] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_DATE] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_DATE] = 0;
 			  } 
		}
		 
		  sprintf(buf, "%06d", (int)PARV_get(handle, cnt, UIC_APPLICATION));
		
		  for (i=0; i<6; i++)
		  {
			  controller_date[i] = buf[i];	  
		  }
}


static void
monitor_get_plan(int *controller_plan)
{
int    i=0;
char   buf[3];

static  PAR_INST  my_plan_pari;

 
  if (! my_plan_pari.id)
  {
      XPARV_openi("PLAN", "SF.SYS", &my_plan_pari);
      if (! my_plan_pari.id)
  		{
      		monitor.status[MONITOR_STS_OPEN_PLAN] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_PLAN] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_PLAN] = 0;
 			  } 
		}
  }

      	sprintf(buf, "%03d", (int)PARV_get(my_plan_pari.id, my_plan_pari.el, UIC_APPLICATION));
		
		  for (i=0; i<3; i++)
		  {
			  controller_plan[i] = buf[i];
		  }
}

static void   
monitor_get_plan_name(int *controller_plan_name)

{
void   *handle_plcmd, *handle_pli;
int    plan_number=0, cnt =0, i=0;
char   buf[14];
char   buf2[14];
	
		handle_plcmd = XPARV_open("PL.CMD");
        if ( ! handle_plcmd)
  		{
      		monitor.status[MONITOR_STS_OPEN_PLAN_NAME] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_PLAN_NAME] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_PLAN_NAME] = 0;
 			  } 
		}
		plan_number = PARV_get(handle_plcmd, cnt, UIC_APPLICATION);
		handle_pli = XPART_open("PL.I");
        if ( ! handle_pli)      
  		{
      		monitor.status[MONITOR_STS_OPEN_PLAN_NAME] = 1;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_PLAN_NAME] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_PLAN_NAME] = 0;
 			  } 
		}
		  if (plan_number>=0)
		  {
			  sprintf(buf2, "%s", PART_get(handle_pli, plan_number, buf, UIC_APPLICATION));
	  
			  do
			  {
					controller_plan_name[i] = buf2[i];
					i++;
			  }
			  while (buf2[i] != 0);
			  for(; i<14; i++)
			  {
				  controller_plan_name[i] = '#';
			  }
		  }
		  else
		  {/* in SMR-2 PL.CMD is -1 and PL.I has no name automatic*/
			  controller_plan_name[0] = 'A';
			  controller_plan_name[1] = 'U';
			  controller_plan_name[2] = 'T';
			  controller_plan_name[3] = 'O';
			  controller_plan_name[4] = 'M';
			  controller_plan_name[5] = 'A';
			  controller_plan_name[6] = 'T';
			  controller_plan_name[7] = 'I';
			  controller_plan_name[8] = 'C';
			  i=9;
			  for(; i<14; i++)
			  {
				  controller_plan_name[i] = '#';
			  }
		  }
}

static void    
monitor_get_alarms(int *controller_alarms)
{
void   *handle, *handle_alarms;
int    alarms_nmbr,i=0,j=0,k=0,l=0;
char   buf[5];
char   buf2[5];
	
		handle = XPARV_open("XSFE.SYS");
        if ( ! handle)
  		{
      		monitor.status[MONITOR_STS_OPEN_ALARMS] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_ALARMS] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_ALARMS] = 0;
 			  } 
		}
		alarms_nmbr = (int)PARV_get(handle, 0, UIC_APPLICATION);
		
		for (i=0; i<alarms_nmbr; i=i*2+1)
		{
			handle_alarms = XPART_open("XSFE.I");
			
   	    	if ( ! handle_alarms)
  			{
      			monitor.status[MONITOR_STS_OPEN_ALARMS] = 1;
  			}
  			else
  			{
	        	  /* Reset the error when a valid service is opened. */
  			  	if (monitor.status[MONITOR_STS_OPEN_ALARMS] != 0)
  			  	{
      		   	    monitor.status[MONITOR_STS_OPEN_ALARMS] = 0;
 			  	} 
			}
			sprintf(buf2, "%s", PART_get(handle_alarms, j, buf, UIC_APPLICATION));
		
			for (l=0; l<4; l++)
			{
		  		controller_alarms[k+l] = buf2[l];
	  		}
		  	k=k+4;
		  	if (j++ >= 7)
		  	{
			  	break;
		  	}
	  	}
	  	for (k=k; k<32; k++)
		{
			controller_alarms[k] = '#';
		}
}

static void    
monitor_get_status(int *controller_status)
{
void   *handle;
int    cnt=0;
char   buf[1];
	
		handle = XPARV_open("MODE.STS");
        if ( ! handle)
  		{
      		monitor.status[MONITOR_STS_OPEN_STATUS] = 1;
      		return;
  		}
  		else
  		{
	          /* Reset the error when a valid service is opened. */
  			  if (monitor.status[MONITOR_STS_OPEN_STATUS] != 0)
  			  {
      		       monitor.status[MONITOR_STS_OPEN_STATUS] = 0;
 			  } 
		}
		sprintf(buf, "%d", (int)PARV_get(handle, cnt, UIC_APPLICATION));
		controller_status[0] = buf[0];
}

/*
 * Function   : monitor_socket_open
 *
 * Abstract   : Creates and opens the socket
 *
 * Decisions  :
 *
 */

static int
monitor_socket_open(void)
{
  monitor.iSocket = XLIB_sock_create(XLIB_SOCK_STREAM, FALSE);
  if (!XLIB_sock_connect(monitor.iSocket, monitor.remotePort, monitor.remoteIp, 5000))
  {
      monitor_socket_close();
      return (FALSE);
  }

  return(TRUE);
}


/*
 * Function   : monitor_socket_close
 *
 * Abstract   : Closes the socket
 *
 * Decisions  :
 *
 */

static void
monitor_socket_close(void)
{
  if (monitor.iSocket)
  {
    XLIB_sock_destroy(monitor.iSocket);
    monitor.iSocket = NULL;
  }
}

/*
 * Function   : monitor_calculate_crc
 *
 * Abstract   : Calculates CRC over part of the frame
 *
 * Decisions  :
 *
 */

static int
monitor_calculate_crc(void)
{
	int i;
	int control_sum;
	
	control_sum = monitor.statusMsg[22];
	
	for (i=23;i<=75;i++)
	{
        control_sum += monitor.statusMsg[i];
    }
    return(control_sum);
}
